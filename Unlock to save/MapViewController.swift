//
//  MapViewController.swift
//  Unlock to save
//
//  Created by gil on 29/03/2021.
//

import UIKit


class MAPViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // en cas de surcharge de mémoire
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if (segue.identifier == "toinviterunami"){
            if let destinationVC = segue.destination as? Inviterunami {
                if let buttonID = sender as? UIButton {
                    destinationVC.projectID = buttonID.restorationIdentifier
                }
            }
            
        }
        
    }
    
    
    @IBAction func dismissHere(segue: UIStoryboardSegue) {
        print("back from:\(String(describing: segue.identifier))")
    }
    
}


