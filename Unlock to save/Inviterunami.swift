//
//  Inviterunami.swift
//  Unlock to save
//
//  Created by Mano on 09/04/2021.
//

import UIKit

class Inviterunami: UIViewController{
    
    var projectID: String?
    var projectInfo = ["PasteurID":"Info projet Paster","Initeru":"Info reseau"]
    
    @IBOutlet weak var infoLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if projectID != nil {
            if let info = self.projectInfo[projectID!] {
                self.infoLabel.text=info
            }
        }
    }
}
